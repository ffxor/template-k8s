const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
const backendUrl = process.env.BACKEND_URL || "http://localhost:5000";
const os = require('os');
console.log();

app.use(function(req, res, next) {
  res.setHeader("Content-Type", "text/html");
  next();
});

app.get("/", (req, res) => {
  const hostname = os.hostname();
  res.end(`<h1>Hello from demo-node @ ${hostname}!</h1><p>Try <a href='/fetch'>/fetch</a> endpoint</p>`);
})

app.get("/fetch", async (req, res) => {
  const response = await fetch(`${backendUrl}/endpoint`);
  const result = await response.text();
  res.end(`Fetched from backend: <pre>${result}</pre>`);
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
