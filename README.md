# Template for Kubernetes

This is training project created during lectures by [Rob Richardson](https://github.com/robrich). Related git repo is https://github.com/robrich/kubernetes-hands-on-workshop/

Here are basic presentations:

- [welcome-to-docker](https://robrich.org/slides/welcome-to-docker/#/)
- [kubernetes-test-drive](https://robrich.org/slides/kubernetes-test-drive/#/)
- [building-stateful-workloads-in-kubernetes](https://robrich.org/slides/building-stateful-workloads-in-kubernetes/#/)

The project is the simplest Node-frontend and Net-backend services, which are being containerized and deployed to k8s cluster.
Nginx is extra reverse proxy added only for docker compose training. Within K8s it's being replaced with the service by `ingress.yml`



## Start

```bash
$ docker compose build
$ kubectl apply -f backend.yml
$ kubectl apply -f frontend.yml
$ kubectl apply -f ingress.yml
```



## Useful commands

### Basic commands

```bash
$ kubectl get all

# Add or Update an element(s)
$ kubectl apply -f FILE.yml

# remove an element(s)
$ kubectl delete -f FILE.yml

# Rolls back K8s configuation to the previous one (i.e. replicas or image tag)
$ kubectl rollout undo DEPLOYMENT_NAME
```

### Service commands

```bash
# Attach console to a Pod
$ kubectl exec --stdin --tty POD_NAME -- sh

# Get logs of a Pod
$ kubectl logs POD_NAME

# Detailed info about Pod/Deploy/Service etc.
$ kubectl describe POD_NAME
```

### Config

```bash
$ kubectl get configmap
$ kubectl get secrets
```

### Debug

```bash
# This tells Kubernetes to setup a tunnel from localhost to the pod directly, skipping the service and ingress.
$ kubectl port-forward POD_NAME 3000:3000

# This tells Kubernetes to setup the tunnel from localhost to the service, and the service will round-robin across the pods.
$ kubectl port-forward SERVICE_NAME 3000:3000
```



## Azure CLI

```bash
# Login into Azure with MS account
$ az login

# Switch active subscription
$ az account list --output table and az account set --subscription {guid}

# Create Kubectl Context
$ az aks get-credentials --resource-group YOUR_GROUP --name CLUSTER_NAME

# Switch Kubectl Context
$ kubectl config get-contexts
$ kubectl config use-context CONTEXT_NAME
```

Also it is possible to build images in cloud instead of local. At this point there is no need to push if afterwards:

```bash
# Build in cloud
$ az acr build --registry REGISTRY_NAME --image IMAGE_TAG .
```