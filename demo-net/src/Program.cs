﻿var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

app.MapGet("/", () => "Hello demo-net!");

app.MapGet("/endpoint", () => "This is endpoint from backend. Try also /endpoint");

app.Run();